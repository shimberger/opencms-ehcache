package net.sf.owt.ehcache;
import java.io.ByteArrayInputStream;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;

import org.opencms.configuration.CmsConfigurationManager;
import org.opencms.file.CmsFile;
import org.opencms.file.CmsObject;
import org.opencms.main.CmsEvent;
import org.opencms.main.CmsException;
import org.opencms.main.I_CmsEventListener;
import org.opencms.main.OpenCms;
import org.opencms.module.A_CmsModuleAction;
import org.opencms.module.CmsModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ModuleClass extends A_CmsModuleAction {

	private static final Logger LOG = LoggerFactory.getLogger(ModuleClass.class);
	
	private static final String DEFAULT_CONFIG = "/system/modules/net.sf.owt.ehcache/config/ehcache.xml";
	
	private static volatile CacheManager manager;
	
	@Override
	public void cmsEvent(CmsEvent event) {
		super.cmsEvent(event);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Received module event " + event.getType());
		}
		if (event.getType() == I_CmsEventListener.EVENT_CLEAR_ONLINE_CACHES ||
			event.getType() == I_CmsEventListener.EVENT_CLEAR_CACHES ||
			event.getType() == I_CmsEventListener.EVENT_PUBLISH_PROJECT) {
			if (manager != null) {
				if (LOG.isInfoEnabled()) {
					LOG.info("Clearing Cache Manager");
				}
				manager.clearAll();
			}
		}
	}

	@Override
	public void initialize(CmsObject adminCms,
			CmsConfigurationManager configurationManager, CmsModule module) {
		super.initialize(adminCms, configurationManager, module);
		startup(adminCms);
		OpenCms.getEventManager().addCmsEventListener(this);
	}

	@Override
	public void shutDown(CmsModule module) {
		super.shutDown(module);
		shutdown();
		OpenCms.getEventManager().removeCmsEventListener(this);
	}
	
	public static CacheManager getCacheManager() {
		return manager;
	}
	
	public static void startup(final CmsObject cmso) {
		if (LOG.isInfoEnabled()) {
			LOG.info("Starting Cache Manager");
		}
		Configuration config = readConfiguration(cmso);
		if (config != null) {
			manager = new CacheManager(config);
		} else {
			if (LOG.isErrorEnabled()) {
				LOG.error("Error starting Cache Manager: No Config");
			}
		}
	}
	
	public static void shutdown() {
		if (LOG.isInfoEnabled()) {
			LOG.info("Stopping Cache Manager");
		}
		manager.shutdown();
		manager = null;
	}
	
	private static Configuration readConfiguration(final CmsObject cmso) {
		if (cmso.existsResource(DEFAULT_CONFIG)) {
			try {
				CmsFile configFile = cmso.readFile(DEFAULT_CONFIG);
				return ConfigurationFactory.parseConfiguration(new ByteArrayInputStream(configFile.getContents()));
			} catch (CmsException e) {
				if (LOG.isErrorEnabled()) {
					LOG.error("Error reading ehcache Configuration",e);
				}
			}
		}
		return null;
	}

}
